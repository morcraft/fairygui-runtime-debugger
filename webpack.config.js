const path = require('path')

const srcPath = (subdir) => {
  return path.join(__dirname, subdir)
}

module.exports = {
  mode: 'production',
  devServer: {
    host: 'localhost',
    port: '3000',
    hot: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    historyApiFallback: true,
  },
  watchOptions: {

    ignored: /node_modules/
  },
  devtool: 'source-map',
  entry: {
    debugger: './src/panel/main.ts',
  },
  output: {
    filename: 'debugger.min.js',
    path: path.join(__dirname, 'dist'),
  },
  
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
    alias: {
      //ui: srcPath('ui'),
    },
  },
  //plugins: [mergeVendor],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
            "style-loader", // creates style nodes from JS strings
            "css-loader", // translates CSS into CommonJS
            "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        },
      }
    ]
  }
}