## FairyGUI Runtime Debugger

A debugger for FairyGUI. Meant to be open in a separate window.

To build this package:

    npm install
    npm run build

The dist folder contains an index.html file, which you may open from any window using the FairyGUI runtime library. It should display all the GRoot instances by default.

open('node_modules/fairygui-runtime-debugger/dist/debugger.html', 'Debugger')

You may append an optional argument 'dependent=yes', to open it in a new window, like this:

open('node_modules/fairygui-runtime-debugger/dist/debugger.html', 'Debugger', 'dependent=yes')

For additional parameters, refer to the [window.open docs](https://developer.mozilla.org/en-US/docs/Web/API/Window/open)