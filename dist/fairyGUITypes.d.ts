declare const types: {
    GBasicTextField: boolean;
    GButton: boolean;
    GComboBox: boolean;
    GComponent: boolean;
    GGraph: boolean;
    GGroup: boolean;
    GImage: boolean;
    GList: boolean;
    GLabel: boolean;
    GLoader: boolean;
    GMovieClip: boolean;
    GObjectPool: boolean;
    GProgressBar: boolean;
    GRichTextField: boolean;
    GRoot: boolean;
    GSlider: boolean;
    GScrollBar: boolean;
    GTextField: boolean;
    GTextInput: boolean;
    GObject: boolean;
};
export declare type fairyGUIElement = keyof typeof types;
export declare type fairyGUIMap = {
    [K in fairyGUIElement]: any;
};
export declare type fairyGUIPartialMap = {
    [K in fairyGUIElement]?: any;
};
export default types;
