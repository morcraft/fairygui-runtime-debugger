export declare const properties: {
    directories: string;
    titlebar: string;
    toolbar: string;
    location: string;
    status: string;
    menubar: string;
    scrollbars: string;
    resizable: string;
    width: number;
    height: number;
    dependent: string;
};
export declare function parseProperties(): string;
