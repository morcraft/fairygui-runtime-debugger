import * as React from 'react';
import View from '../view';
interface Props {
    node: View;
}
interface State {
    visible: boolean;
}
export default class TreeNodeVisibilityToggler extends React.Component<Props, State> {
    constructor(props: any);
    onClick: () => void;
    render(): JSX.Element;
}
export {};
