import * as React from 'react';
import View from '../view';
interface State {
}
interface Props {
    view: View;
    recursive: boolean;
    inspectedTree: fairygui.GObject[];
}
export default class Tree extends React.Component<Props, State> {
    render(): JSX.Element;
}
export {};
