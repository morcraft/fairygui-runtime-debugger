import * as React from 'react';
import View from '../view';
interface Props {
    node: View;
}
interface State {
}
export default class TreeNodeLaya extends React.Component<Props, State> {
    onClick: (event: any) => void;
    render(): JSX.Element;
}
export {};
