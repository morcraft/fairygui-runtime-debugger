import * as React from 'react';
import View from '../view';
interface Props {
    node: View;
}
interface State {
    copied: boolean;
}
export default class TreeNodeTextToClipboard extends React.Component<Props, State> {
    constructor(props: any);
    onClick: () => void;
    onCopy: () => void;
    render(): JSX.Element;
}
export {};
