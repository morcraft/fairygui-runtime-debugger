import * as React from 'react';
import { fairyGUIMap } from '../../fairyGUITypes';
import View from '../view';
export declare const iconMap: fairyGUIMap;
interface Props {
    node: View;
}
interface State {
    node: View;
}
export default class TreeNodeIcon extends React.Component<Props, State> {
    constructor(props: any);
    onClick: (event: any) => void;
    render(): JSX.Element;
}
export {};
