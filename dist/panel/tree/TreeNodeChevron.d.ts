import * as React from 'react';
import View from '../view';
interface State {
    children: fairygui.GComponent[];
}
interface Props {
    recursive: boolean;
    node: View;
    onClick: (event: React.MouseEvent<HTMLDivElement>) => void;
    folded: boolean;
}
export default class TreeNodeChevron extends React.Component<Props, State> {
    constructor(props: any);
    render(): JSX.Element;
}
export {};
