import * as React from 'react';
interface State {
    node: fairygui.GComponent;
    folded: boolean;
    highlight: boolean;
    recursive: boolean;
    inspectedTree: fairygui.GComponent[];
}
interface Props {
    node: fairygui.GComponent;
    folded: boolean;
    highlight: boolean;
    recursive: boolean;
    inspectedTree: fairygui.GComponent[];
}
export default class TreeNode extends React.Component<Props, State> {
    constructor(props: any);
    componentWillReceiveProps(props: any): void;
    onClick: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    onMouseEnter: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    onMouseLeave: (event: any) => void;
    onTreeNodeChevronClick: (event: any) => void;
    render(): JSX.Element;
}
export {};
