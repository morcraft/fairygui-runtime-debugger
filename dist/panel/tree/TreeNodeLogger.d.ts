import * as React from 'react';
import View from '../view';
interface Props {
    node: View;
}
interface State {
}
export default class TreeNodeLogger extends React.Component<Props, State> {
    onClick: () => void;
    render(): JSX.Element;
}
export {};
