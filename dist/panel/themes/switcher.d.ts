import All from './all';
export declare type ThemeName = keyof typeof All;
export default class ThemeSwitcher {
    static HTMLStyle: CSSStyleDeclaration;
    static switch: (themeName: "Dracula" | "Nord" | "Tomorrow" | "Zenburn" | "SolarizedDark" | "SolarizedLight" | "UnikittyDark" | "UnikittyLight") => void;
}
