import './App.scss';
export default class App {
    readonly view: fairygui.GComponent;
    constructor(view: fairygui.GComponent, autostart?: boolean);
    render: () => void;
    remove: () => void;
}
