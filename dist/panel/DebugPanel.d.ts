import * as React from 'react';
import View from './view';
interface State {
    view: View;
    originalView: fairygui.GRoot;
    recursive: boolean;
    inspectedTree: fairygui.GObject[];
    dockedTo: string;
}
interface Props {
    view: View;
}
export default class DebugPanel extends React.Component<Props, State> {
    constructor(props: any);
    onFilterClean: (event: any) => void;
    onFilter: (event: any, filteredElements: any) => void;
    scrollInspectedElementIntoView: () => void;
    afterInspect: () => void;
    onInspect: (element: fairygui.GObject) => void;
    onDockChange: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    render(): JSX.Element;
}
export {};
