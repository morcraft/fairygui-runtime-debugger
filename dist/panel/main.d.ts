import App from './App';
export declare const inheritedVariables: string[];
export default class Debugger {
    DebugPanel: typeof App;
    opener: any;
    window: any;
    debugger: App;
    triggerUnload: boolean;
    constructor(view: fairygui.GRoot, autostart?: boolean);
    log(...messages: string[]): void;
    addEvents(): void;
    load(view: fairygui.GComponent): void;
    unload(): void;
}
