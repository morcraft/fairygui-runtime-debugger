import * as React from 'react';
import View from '../view';
import { filter } from './filterByOptions';
export declare const filterByOptions: {
    name: (container: fairygui.GComponent, name: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean) => any[];
    type: (container: fairygui.GComponent, text: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean) => any[];
    text: (container: fairygui.GComponent, text: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean) => any[];
};
interface Props {
    onFilter: Function;
    onFilterClean: Function;
    view: View;
    filterBy: filter;
}
interface State {
    view: fairygui.GComponent;
    filterBy: filter;
    filteredElements: fairygui.GObject[];
    filterCriterion: string;
}
export default class FilterTree extends React.Component<Props, State> {
    constructor(props: any);
    filter: (event: any) => void;
    onInputKeyUp: (event: any) => void;
    onDropdownChange: (event: any) => void;
    render(): JSX.Element;
}
export {};
