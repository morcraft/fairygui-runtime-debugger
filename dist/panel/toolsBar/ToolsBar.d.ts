import * as React from 'react';
import { filter } from './filterByOptions';
import View from '../view';
interface Props {
    view: View;
    onInspect: Function;
    onFilter: Function;
    onFilterClean: Function;
}
interface State {
    filterBy: filter;
    view: View;
}
export default class TreeNode extends React.Component<Props, State> {
    constructor(props: any);
    render(): JSX.Element;
}
export {};
