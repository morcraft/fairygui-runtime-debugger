import * as React from 'react';
import { direction } from './directionParser';
export declare const valuesMap: {
    bottom: string;
    left: string;
    right: string;
};
export declare const iconMap: {
    bottom: string;
    left: string;
    right: string;
};
interface Props {
    onChange: Function;
    direction: direction;
    dockTo: string;
}
interface State {
    dockTo: string;
}
export default class DockTo extends React.Component<Props, State> {
    constructor(props: any);
    onChange: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    render(): JSX.Element;
}
export {};
