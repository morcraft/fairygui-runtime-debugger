export declare const directionsMap: {
    down: boolean;
    right: boolean;
    up: boolean;
    left: boolean;
};
export declare type direction = keyof typeof directionsMap;
export default class DirectionParser {
    private static directions;
    static getDirection: (direction: any) => any;
}
