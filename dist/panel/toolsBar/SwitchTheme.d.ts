import * as React from 'react';
import { ThemeName } from '../themes/switcher';
import { direction } from './directionParser';
interface Props {
    currentTheme: ThemeName;
    direction: direction;
}
interface State {
    currentTheme: ThemeName;
}
export default class SwitchTheme extends React.Component<Props, State> {
    constructor(props: any);
    onThemeSwitch: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    getThemeOptions: () => {};
    render(): JSX.Element;
}
export {};
