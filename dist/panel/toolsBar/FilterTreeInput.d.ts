import * as React from 'react';
export declare const filterByOptions: {
    name: string;
    type: string;
    text: string;
};
interface Props {
    filterBy: keyof typeof filterByOptions;
    onKeyUp: (event: React.KeyboardEvent<HTMLDivElement>) => void;
}
interface State {
}
export default class FilterTreeDropdown extends React.Component<Props, State> {
    render(): JSX.Element;
}
export {};
