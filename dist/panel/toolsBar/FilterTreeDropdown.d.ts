import * as React from 'react';
import { direction } from './directionParser';
import { filter } from './filterByOptions';
interface Props {
    direction: direction;
    onChange: Function;
    filterBy: filter;
}
interface State {
}
export default class FilterTreeDropdown extends React.Component<Props, State> {
    render(): JSX.Element;
}
export {};
