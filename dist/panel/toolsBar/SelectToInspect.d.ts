import * as React from 'react';
import View from '../view';
interface Props {
    view: View;
    onInspect: Function;
}
interface State {
    elementsPrepared: boolean;
    enabled: boolean;
    view: View;
    children: fairygui.GObject[];
}
export default class SelectToInspect extends React.Component<Props, State> {
    constructor(props: any);
    onInspect: (event: Laya.Event) => void;
    onClick: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    render(): JSX.Element;
}
export {};
