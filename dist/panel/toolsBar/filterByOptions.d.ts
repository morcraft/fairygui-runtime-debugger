declare const filterByOptions: {
    name: string;
    type: string;
    text: string;
};
export declare type filter = keyof typeof filterByOptions;
export default filterByOptions;
