export default class Utils {
    private static elementHighlightBox;
    private static PropertiesText;
    private static RelevantProperties;
    static filters: {
        byName: (a: fairygui.GObject, b: string) => boolean;
        byText: (a: fairygui.GObject, b: string) => boolean;
        byType: (a: any, b: string) => boolean;
    };
    private static children;
    static getCubeLines: (e: fairygui.GComponent) => {
        fromX: number;
        toX: number;
        fromY: number;
        toY: number;
    }[];
    static drawStats: (view: fairygui.GComponent, process?: boolean) => void;
    static getAbsoluteDimensions: (element: fairygui.GComponent) => {
        x: number;
        y: number;
        width: number;
        height: number;
    };
    static drawBorder: (element: fairygui.GComponent) => void;
    static processElement: (element: fairygui.GComponent) => void;
    static findElement: (container: fairygui.GComponent, text: string, filter: Function, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean) => any[];
    static findElementByName: (container: fairygui.GComponent, name: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean) => any[];
    static findElementByText: (container: fairygui.GComponent, text: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean) => any[];
    static findElementByType: (container: fairygui.GComponent, text: string, highlight?: boolean, cleanAfter?: number, showFeedback?: boolean) => any[];
    static getChildren: (container: fairygui.GComponent, recursive?: boolean, process?: boolean) => fairygui.GComponent[];
    static highlightInspectedEvent: (event: any) => void;
    static removeInspectionEvent: (event: any) => void;
    static addInspectionListeners: (element: fairygui.GObject, onInspect: Function) => void;
    static removeInspectionListeners: (element: fairygui.GObject, onInspect: Function) => void;
    static cleanLines: () => void;
    static cleanPropertiesText: () => void;
    static getRelevantProperties: (element: fairygui.GComponent) => {
        formatted: string;
        properties: any;
    };
    static createFormattedPropertiesBox: (element: fairygui.GComponent) => Laya.Text;
    static drawBoxAroundElement: (element: fairygui.GComponent) => Laya.Sprite;
    static debugElement: (element: fairygui.GComponent, event?: Laya.Event, log?: boolean) => void;
    static addListeners: (view: fairygui.GComponent) => void;
}
