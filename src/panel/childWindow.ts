export const properties = {
    directories:'no',
    titlebar: 'yes',
    toolbar: 'no',
    location: 'no',
    status: 'no',
    menubar: 'no',
    scrollbars: 'yes',
    resizable: 'no',
    width: window.innerWidth / 2,
    height: window.innerHeight / 2,
    dependent: 'yes',
}

export function parseProperties(){
    let propString = ''
    for(const prop in properties){
        propString += prop + '=' + properties[prop] + ','
    }
    return propString
}