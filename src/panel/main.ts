import App from './App'
import DebugUtils from '../utils'

export const inheritedVariables = [
    'Laya',
    'laya',
    'fairygui',
    'console',
    'alert'
]

export default class Debugger {
    DebugPanel = App
    opener: any
    window: any
    debugger: App
    triggerUnload = true

    constructor(view: fairygui.GRoot, autostart?: boolean){
        if(!window.opener)
            throw new Error(`Couldn't find opener window. The debugger is meant to be opened from a second window.`)
            
        if(autostart !== false){
            this.window = window
            this.opener = window.opener

            if(this.opener.__FairyGUIDebuggerInstance){
                this.log(`There's already a debugger instance opened`)
                this.triggerUnload = false
                this.window.close()
            }

            inheritedVariables.forEach(property => {
                this.window[property] = this.opener[property]
            })

            this.addEvents()
            this.load(view)
        }
    }

    log(...messages: string[]){
        this.opener.console.log(`%c ${messages}`, 'background: #3a3c4e; color: #ea51b2')
    }

    addEvents(){
        window.addEventListener('unload', () => {
            if(this.triggerUnload){
                DebugUtils.cleanLines()
                DebugUtils.cleanPropertiesText()
                this.unload()

                delete this.opener.__FairyGUIDebuggerInstance
            }
        })
    }

    load(view: fairygui.GComponent){
        this.debugger = new App(view)
        this.opener.__FairyGUIDebuggerInstance = this.debugger
        this.log('Debugger opened')
    }

    unload(){
        this.debugger.remove()
        this.log('Debugger closed')
    }
}

try{
    const CoolDebugger = new Debugger(opener.fairygui.GRoot.inst)
}
catch(error){
    const h3 = document.createElement('h3')
    const textNode = document.createTextNode(error)
    h3.appendChild(textNode)
    h3.classList.add('error-message')
    document.body.appendChild(h3)
}