import * as React from 'react'
import Select from '../generic/Select'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library, IconProp } from '@fortawesome/fontawesome-svg-core'
import { faWindowMaximize, faAngleDown, faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons'
library.add(faWindowMaximize, faAngleDown, faAngleLeft, faAngleRight)
import { direction } from './directionParser'

export const valuesMap = {
  bottom: 'Dock To Bottom',
  left: 'Dock To Left', 
  right: 'Dock To Right', 
}

export const iconMap = {
  bottom: 'angle-down',
  left: 'angle-left',
  right: 'angle-right'
}

interface Props{
  onChange: Function
  direction: direction
  dockTo: string
}

interface State{
  dockTo: string
}

export default class DockTo extends React.Component<Props, State> {
    constructor(props){
      super(props)
      this.state = {
        dockTo: Object.keys(valuesMap)[0]
      }
    }

    onChange = (event: React.MouseEvent<HTMLElement>) => {
      this.setState({
        dockTo: event.currentTarget.dataset.value
      })
      this.props.onChange(event)
    }

    render(){

      const className = `dock-to-dropdown dock-to ${this.props.dockTo}`
      const dockedTo = this.state.dockTo
      const icon = iconMap[dockedTo] as IconProp
      return <div className='dock-to'>
        <span>Docked to {dockedTo}</span>
        <Select 
          options={valuesMap} 
          classes={className} 
          direction={this.props.direction} 
          onChange={this.onChange}
        />
      </div>
    }
}