import * as React from 'react'
import Select from '../generic/Select'
import { direction } from './directionParser'
import filterByOptions, { filter } from './filterByOptions'

interface Props{
  direction: direction
  onChange: Function
  filterBy: filter
}

interface State{}

export default class FilterTreeDropdown extends React.Component<Props, State> {
    render() {  
      const className = `filter-dropdown filter-by ${this.props.filterBy}`
      return (
        <Select 
          options={filterByOptions} 
          classes={className} 
          direction={this.props.direction} 
          onChange={this.props.onChange}
        />
      )
    }
  }