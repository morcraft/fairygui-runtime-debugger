export const directionsMap = {
    down: true,
    right: true,
    up: true,
    left: true,
}

export type direction = keyof typeof directionsMap

export default class DirectionParser {
    private static directions = ['down', 'right', 'up', 'left']

    public static getDirection = (direction: any) => {
        let finalDirection
        if(typeof direction != 'string'){
            finalDirection = DirectionParser.directions[0]
        }
        else{
            if(DirectionParser.directions.indexOf(direction) < 0){
                console.info(`Invalid direction "${direction}". Assigning default.`)
                finalDirection = DirectionParser.directions[0]
            }
            else{
                finalDirection = direction
            }
        }
        return finalDirection
    }
  }