import * as React from 'react'
import DebugUtils from '../../utils'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMagic } from '@fortawesome/free-solid-svg-icons'
library.add(faMagic)
import View from '../view'

interface Props{
  view: View
  onInspect: Function
}

interface State{
  elementsPrepared: boolean
  enabled: boolean
  view: View
  children: fairygui.GObject[]
}

export default class SelectToInspect extends React.Component<Props, State> {
    constructor(props){
      super(props)
      this.state = {
        elementsPrepared: false,
        enabled: false,
        view: props.view,
        children: []
      }
    }

    onInspect = (event: Laya.Event) => {
      //@ts-ignore //Property added by fairyGUI
      this.props.onInspect(event.target.$owner)
      this.setState({
        enabled: false
      })

      DebugUtils.removeInspectionListeners(this.props.view._children[0], this.onInspect)

      //return false
    }

    onClick = (event: React.MouseEvent<HTMLElement>) => {
      if(!this.state.elementsPrepared){
        this.setState({
          elementsPrepared: true,
          children: DebugUtils.getChildren(this.props.view as fairygui.GComponent, true, true),//prepare elements
        })
      }

      DebugUtils.cleanLines()
      DebugUtils.cleanPropertiesText()

      this.setState({
        enabled: !this.state.enabled,
      }, () => {
        if(this.state.enabled){
          DebugUtils.addInspectionListeners(this.props.view._children[0], this.onInspect)
        }
        else{
          DebugUtils.removeInspectionListeners(this.props.view._children[0], this.onInspect)
        }
      })
    }

    render() {  
      const className = `select-to-inspect ${this.state.enabled ? 'toggled-on' : ''}`
      const title =`Click to inspect elements on the stage`
      return (
        <div title={title} onClick={this.onClick} className={className}>
          <FontAwesomeIcon icon='magic'/>
        </div>
      )
    }
  }