import * as React from 'react'
import DebugUtils from '../../utils'
import FilterTreeDropdown from './FilterTreeDropdown'
import FilterTreeInput from './FilterTreeInput'
import View from '../view'
import { filter } from './filterByOptions'

export const filterByOptions = {
  name: DebugUtils.findElementByName,
  type: DebugUtils.findElementByType,
  text: DebugUtils.findElementByText
}

interface Props{
  onFilter: Function
  onFilterClean: Function
  view: View
  filterBy: filter
}

interface State{
  view: fairygui.GComponent
  filterBy: filter
  filteredElements: fairygui.GObject[]
  filterCriterion: string
}

export default class FilterTree extends React.Component<Props, State> {
    constructor(props){
      super(props)
      this.state = {
        view: props.view,
        filterBy: props.filterBy,
        filteredElements: props.view._children,
        filterCriterion: ''
      }
    }

    filter = (event) => {
      const filteredElements = filterByOptions[this.state.filterBy](this.state.view, this.state.filterCriterion, false, 0, false)
      this.setState({
        filteredElements: filteredElements
      }, () => {
        this.props.onFilter(event, this.state.filteredElements)
      })
    }

    onInputKeyUp = event =>{
      const value = event.target.value
      this.setState({
        filterCriterion: value
      }, () => {
        if(value.length){
          this.filter(event)
        }
        else{
          this.props.onFilterClean()
        }
      })
    }

    onDropdownChange = event => {
      const target = event.currentTarget
      this.setState({
        filterBy: target.dataset.value
      }, () => {
        if(this.state.filterCriterion.length){
          this.filter(event)
        }
      })
    }
  
    render() {  
      return (
        <div className='filter-tree'>
          <FilterTreeInput
            onKeyUp={this.onInputKeyUp} 
            filterBy={this.state.filterBy}
          />
          <FilterTreeDropdown
            onChange={this.onDropdownChange} 
            direction='down' 
            filterBy={this.state.filterBy}
          /> 
        </div>
      )
    }
  }