import * as React from 'react'
import FilterTree from './FilterTree'
import SelectToInspect from './SelectToInspect'
//import DockTo from './DockTo'
import SwitchTheme from './SwitchTheme'
import { theme as Theme } from '../themes/all'
import { filter } from './filterByOptions'
import View from '../view'

interface Props{
  view: View
  onInspect: Function
  onFilter: Function
  onFilterClean: Function
}

interface State{
  filterBy: filter
  view: View
}

export default class TreeNode extends React.Component<Props, State> {
    constructor(props){
      super(props)
      this.state = {
        view: props.view,
        filterBy: 'name'
      }
    }

    render() {  
      const view = this.props.view
      const theme: Theme = 'Nord'
      return (
        <div className='tools-bar'>
            <SelectToInspect
              view={view}
              onInspect={this.props.onInspect}
              />
            <FilterTree 
              view={view} 
              filterBy={this.state.filterBy} 
              onFilter={this.props.onFilter} 
              onFilterClean={this.props.onFilterClean}
            />
            <SwitchTheme 
              currentTheme={theme}
              direction='down'
            />
            {/*<DockTo //looks ugly af
              onChange={this.props.onDockChange}
            />*/}
        </div>
      )
    }
  }