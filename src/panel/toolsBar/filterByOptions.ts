const filterByOptions = {
    name: 'Name', 
    type: 'Type', 
    text: 'Text',
}

export type filter = keyof typeof filterByOptions

export default filterByOptions