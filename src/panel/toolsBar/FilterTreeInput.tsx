import * as React from 'react'

export const filterByOptions = {
  name: 'Name', 
  type: 'Type', 
  text: 'Text',
}

interface Props{
  filterBy: keyof typeof filterByOptions
  onKeyUp: (event: React.KeyboardEvent<HTMLDivElement>) => void
}

interface State{
        
}

export default class FilterTreeDropdown extends React.Component<Props, State> {
    render() {  
      const placeholder = `Filter by `
      const className = `filter filter-by ${this.props.filterBy}`
      return (
        <input 
            onKeyUp={this.props.onKeyUp} 
            className={className}
            type='text' 
            placeholder={placeholder}
        />
      )
    }
  }