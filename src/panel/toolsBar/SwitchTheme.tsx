import * as React from 'react'
import AllThemes, { theme } from '../themes/all'
import Select from '../generic/Select'
import ThemeSwitcher, { ThemeName } from '../themes/switcher'
import { direction } from './directionParser'

interface Props{
  currentTheme: ThemeName
  direction: direction
}

interface State{
  currentTheme: ThemeName
}

export default class SwitchTheme extends React.Component<Props, State> {
  constructor(props){
    super(props)
    this.state = {
      currentTheme: this.props.currentTheme
    }
  }

  onThemeSwitch = (event: React.MouseEvent<HTMLElement>) => {
    const value = event.currentTarget.dataset.value as ThemeName
    ThemeSwitcher.switch(value as ThemeName)
    this.setState({
      currentTheme: value
    })
  }

  getThemeOptions = () => {
    let availableThemes = {}
    for(const prop in AllThemes){
      availableThemes[prop] = prop
    }
    return availableThemes
  }

  render() {  
    const className = 'switch-theme'
    const themeOptions = this.getThemeOptions()
    return (
      <div className='switch-theme' title='Theme picker'>
        <span className='current-theme-name'>{this.state.currentTheme}</span>
        <Select
          options={themeOptions}
          classes={className} 
          direction={this.props.direction} 
          onChange={this.onThemeSwitch}
        />
      </div>
    )
  }
}