import * as React from 'react'
import Tree from './tree/Tree'
import ToolsBar from './toolsBar/ToolsBar'
import View from './view'

interface State{
    view: View
    originalView: fairygui.GRoot
    recursive: boolean
    inspectedTree: fairygui.GObject[]
    dockedTo: string
  }
  
interface Props{
    view: View
}
  
export default class DebugPanel extends React.Component<Props, State>{
    constructor(props){
      super(props)
      this.state = {
        view: props.view,
        originalView: props.view,
        recursive: true,
        inspectedTree: [],
        dockedTo: 'bottom',
      }
    }
  
    onFilterClean = (event) => {
      this.setState({
        view: this.state.originalView,
      })
      //this.props.render()
    }
  
    onFilter = (event, filteredElements) =>{
      this.setState({
        view: {
          _children: filteredElements,
        } as fairygui.GRoot,
      })
    }
  
    scrollInspectedElementIntoView = () => {
      const element = document.querySelector('.tree-node.highlight')
      if(element instanceof Node){
        element.scrollIntoView({
          inline: 'center'
        })
      }
    }
  
    afterInspect = () => {
      this.scrollInspectedElementIntoView()
    }
  
    onInspect = (element: fairygui.GObject) => {
      const nodeChain = [element]
      let parent = element.parent
  
      while(parent === Object(parent)){
        nodeChain.unshift(parent) //push into first slot
        parent = parent.parent
      }
  
      this.setState({
        inspectedTree: nodeChain,
        view: this.state.originalView
      }, this.afterInspect)  
    }
  
    onDockChange = (event: React.MouseEvent<HTMLElement>) => {
      this.setState({
        dockedTo: event.currentTarget.dataset.value
      })
    }
  
    render() {
      return <div className='debug-panel' data-docked-to={this.state.dockedTo}>
        <ToolsBar 
          view={this.state.view} 
          onFilter={this.onFilter} 
          onFilterClean={this.onFilterClean}
          onInspect={this.onInspect}
          //onDockChange={this.onDockChange}
        />
        <div className='tree-container'>
          <Tree 
            view={this.state.view} 
            recursive={this.state.recursive} 
            inspectedTree={this.state.inspectedTree}
          />
        </div>
      </div>
    }
}