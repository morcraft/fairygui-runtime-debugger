export default interface View{
    _children: fairygui.GComponent[]
    [s: string]: any
}