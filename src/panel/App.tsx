import * as React from 'react'
import * as ReactDOM from 'react-dom'
import DebugPanel from './DebugPanel'
//import 'font-awesome/css/font-awesome.css';
import './App.scss'

export default class App{
  constructor(readonly view: fairygui.GComponent, autostart?: boolean){
    if(autostart !== false)
      this.render()
  }

  render = () => {
    const rootNode = document.createElement('div')
    rootNode.classList.add('debugger-root')
    const body = document.body || document.getElementsByTagName('body')[0]
    body.appendChild(rootNode)
    console.log(body, document, rootNode)

    ReactDOM.render(
      <DebugPanel view={this.view}/>,
      rootNode
    )
  }

  remove = () => {
    ReactDOM.unmountComponentAtNode(document.querySelector('.debugger-root'))
  }
}