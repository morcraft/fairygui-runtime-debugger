import Dracula from './Dracula'
import Nord from './Nord'
import SolarizedDark from './SolarizedDark'
import SolarizedLight from './SolarizedLight'
import Tomorrow from './Tomorrow'
import UnikittyDark from './UnikittyDark'
import UnikittyLight from './UnikittyLight'
import Zenburn from './Zenburn'

const themes = {
    Dracula,
    Nord,
    SolarizedDark,
    SolarizedLight,
    Tomorrow,
    UnikittyDark,
    UnikittyLight,
    Zenburn,
}

export type theme = keyof typeof themes

export default themes