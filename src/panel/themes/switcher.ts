import All from './all'

export type ThemeName = keyof typeof All

export default class ThemeSwitcher{
    public static HTMLStyle = document.getElementsByTagName('html')[0].style
    public static switch = (themeName: ThemeName) => {
        const theme = All[themeName]
        Object.keys(theme).forEach((property, index) => {
            ThemeSwitcher.HTMLStyle.setProperty(`--${property}`, theme[property])
        })
    }
}