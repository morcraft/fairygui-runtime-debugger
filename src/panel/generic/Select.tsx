import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library, IconProp } from '@fortawesome/fontawesome-svg-core'
import { faCaretRight, faCaretDown, faCaretUp, faCaretLeft } from '@fortawesome/free-solid-svg-icons'
library.add(faCaretRight, faCaretDown, faCaretUp, faCaretLeft)
import DirectionParser, { direction } from '../toolsBar/directionParser'
import onClickOutside from 'react-onclickoutside'

interface Props{
    direction: direction
    toggledOn: boolean
    options: any
    onChange: Function
    classes?: string
}

interface State{
    toggledOn: boolean
    direction: direction
}

class Select extends React.Component<Props, State> {
    constructor(props){
        super(props)
        this.state = {
            direction: this.props.direction,
            toggledOn: typeof this.props.toggledOn == 'boolean' ? this.props.toggledOn : false
        }
    }

    handleClickOutside = event => {
        this.setState({
            toggledOn: false
        })
    }

    onHeadClick = (event: React.MouseEvent<HTMLElement>) => {
        this.setState({
            toggledOn: !this.state.toggledOn
        })
    }

    onOptionClick = (event: React.MouseEvent<HTMLElement>) => {
        this.props.onChange(event)
        this.setState({
            toggledOn: false
        })
    }

    makeOptions = () => {
        const options = []
        if(Array.isArray(this.props.options) || this.props.options === Object(this.props.options)){
            for(const key in this.props.options){
                options.push(
                    <div key={key} className='option unselectable' data-value={key} onClick={this.onOptionClick}>
                        <span>{this.props.options[key]}</span>
                    </div>
                )
            }
        }
        return options
    }


    render() {  
        const direction = DirectionParser.getDirection(this.state.direction)
        let className = `dropdown ${direction} `
        className += this.state.toggledOn ? 'toggled-on ' : ''
        if(typeof this.props.classes == 'string'){
            className += this.props.classes  
        }
        const options = this.makeOptions()

        return (
            <div className={className} title='Click to toggle'>
                <div className='head' onClick={this.onHeadClick}>
                    <div className={`caret ${direction}`}>
                        <FontAwesomeIcon icon={`caret-${direction}` as IconProp}/>
                    </div>
                </div>
                <div className='options'>
                    {options}
                </div>
            </div>
        )
    }
}

export default onClickOutside(Select)