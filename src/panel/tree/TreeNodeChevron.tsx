import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCaretRight, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import View from '../view'

library.add(faCaretRight, faCaretDown)

interface State{
  children: fairygui.GComponent[]
}

interface Props{
  recursive: boolean
  node: View
  onClick: (event: React.MouseEvent<HTMLDivElement>) => void 
  folded: boolean
}

export default class TreeNodeChevron extends React.Component<Props, State> {
  constructor(props){
    super(props)
    this.state = {
      children: props.node.children
    }
  }

  render() {  
    if(!this.props.recursive || !this.props.node || !Array.isArray(this.props.node._children) || !this.props.node._children.length)
      return null

    return(
      <div className='chevron' onClick={this.props.onClick}>
        {this.props.folded ? 
          <FontAwesomeIcon icon='caret-right'/> : 
          <FontAwesomeIcon icon='caret-down'/>
        }
      </div>
    )
  }
}