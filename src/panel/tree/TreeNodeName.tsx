import * as React from 'react'
import { iconMap } from './TreeNodeIcon'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCodeBranch } from '@fortawesome/free-solid-svg-icons'
library.add(faCodeBranch)
import View from '../view'

interface Props{
  node: View
}

interface State{}

export default class TreeNodeChildrenNumber extends React.Component<Props, State> {
  onClick = (event) => {
    console.info(this.props.node.children)
  }

  render() {  
    if(!this.props.node) 
        return null

    //@ts-ignore
    let name = this.props.node.constructor.name
    if(typeof iconMap[name] != 'string'){//Minified FairyGUI?
      //@ts-ignore
      name = this.props.node.constructor.__className.split('.')[1]
    }

    if(typeof iconMap[name] != 'string'){
      //console.info(`Couldn't determine constructor name for node `, this.props.node, `Assigning default.`)
      name = 'GObject'
    }


    return(
        <span className='unselectable'>
          <b>{this.props.node.name}</b> {`<${name}>`}
        </span>
    )
  }
}