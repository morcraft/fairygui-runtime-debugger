import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMicrochip } from '@fortawesome/free-solid-svg-icons'
import View from '../view'

library.add(faMicrochip)

interface Props{
  node: View
}

interface State{}

export default class TreeNodeLaya extends React.Component<Props, State> {
    onClick = (event) => {
        console.info(this.props.node.displayObject)
    }

    render() {  
      const title = `Click to see Laya's displayObject`
      return(
        <div className='laya' onClick={this.onClick} title={title}>
            <FontAwesomeIcon icon='microchip'/>
        </div>
      )
  }
}