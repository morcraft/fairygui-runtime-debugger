import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSitemap } from '@fortawesome/free-solid-svg-icons'
import View from '../view'

library.add(faSitemap)

interface Props{
  node: View
}

interface State{}

export default class TreeNodeControllerNumber extends React.Component<Props, State> {
    onClick = (event) => {
        console.info(this.props.node.controllers)
    }

    render() {  
      if(!this.props.node || !this.props.node.controllers || !this.props.node.controllers.length) 
        return null
        
      const number = this.props.node.controllers.length
      const title = `This node has ${number} ${(number > 1 ? 'controllers' : 'controller')}. Click to see in console.`
      return(
        <div className='controllers' onClick={this.onClick} title={title}>
            <FontAwesomeIcon icon='sitemap'/>
            <span>{number}</span>
        </div>
      )
  }
}