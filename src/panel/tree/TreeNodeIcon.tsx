import * as React from 'react'
import { fairyGUIMap } from '../../fairyGUITypes'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faImage, faStop, faFont, faDrawPolygon, faListUl, 
         faObjectGroup, faLayerGroup, faSpinner, faFilm,
         faSlidersH,  faGripLines, faKeyboard, faBox, faLongArrowAltRight,
         faAlignLeft, faQuoteRight, faChevronDown, faTextWidth, faObjectUngroup,
        faQuestion} 
  from '@fortawesome/free-solid-svg-icons'

import View from '../view'

library.add(
  faImage, faStop, faFont, faDrawPolygon, faListUl, 
  faObjectGroup, faLayerGroup, faSpinner, faFilm, faSlidersH,
  faGripLines, faKeyboard, faBox, faLongArrowAltRight,faAlignLeft, 
  faQuoteRight, faChevronDown, faTextWidth, faObjectUngroup,
  faQuestion
)

export const iconMap: fairyGUIMap = {
  GBasicTextField: 'quote-right',
  GButton: 'stop',
  GComboBox: 'chevron-down',
  GComponent: 'layer-group',
  GGraph: 'draw-polygon',
  GGroup: 'object-group',
  GImage: 'image',
  GList: 'list-ul',
  GLabel: 'font',
  GLoader: 'spinner',
  GMovieClip: 'film',
  GObjectPool: 'object-ungroup',
  GProgressBar: 'long-arrow-alt-right',
  GRichTextField: 'align-left',
  GRoot: 'box',
  GSlider: 'sliders-h',
  GScrollBar: 'grip-lines',
  GTextField: 'text-width',
  GTextInput: 'keyboard',
  GObject: 'question'
}

interface Props{
  node: View
}

interface State{
  node: View
}

export default class TreeNodeIcon extends React.Component<Props, State> {
  constructor(props){
    super(props)
    this.state = {
      node: props.node,
    }
  }

  onClick = (event) => {
    console.log(this.props.node.constructor)
  }

  render() {  
    const node = this.props.node
    //@ts-ignore
    let name = node.constructor.name
    let icon = iconMap[name]
    if(typeof icon != 'string'){//minified FairyGUI?
      //@ts-ignore
      name = node.constructor.__className.split('.')[1]
      icon = iconMap[name]
    }

    if(typeof icon != 'string'){
      //console.info(`Couldn't find icon for node `, node, `based on constructor name. Is FairyGUI minified?. Assigning default.`)
      icon = iconMap.GObject
    }

    const title = `${name}. Click to view constructor in console.`
    return (
        <div className='icon' onClick={this.onClick} title={title}>
            <FontAwesomeIcon icon={icon}/>
        </div>
    )
  }
}