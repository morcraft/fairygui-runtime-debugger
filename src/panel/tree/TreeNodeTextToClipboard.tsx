import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faClipboard } from '@fortawesome/free-solid-svg-icons'
library.add(faClipboard)
import {CopyToClipboard} from 'react-copy-to-clipboard'
import View from '../view'

interface Props{
    node: View
}

interface State{
    copied: boolean
}

export default class TreeNodeTextToClipboard extends React.Component<Props, State> {
    constructor(props){
        super(props)
        this.state = {
            copied: false
        }
    }
    onClick = () => {
        console.log('Going to copy', this.props.node.text)
    }

    onCopy = () => {
        this.setState({
            copied: true
        })
        console.log(`Copied node's text "${this.props.node.text} to clipboard."`)
    }

    render() {  
        if(!this.props.node || !this.props.node.text || !this.props.node.text.length)
            return null

        let className = 'copy-to-clipboard tool '
        if(this.state.copied){
            className += 'copied'
        }

        return (
            <CopyToClipboard text={this.props.node.text}
                onCopy={this.onCopy}>
                <div className={className} title={`Copy node's text to clipboard`} onClick={this.onClick}>
                    <FontAwesomeIcon icon='clipboard'/>
                </div>  
            </CopyToClipboard>
        )
    }
}