import * as React from 'react'
import TreeNode from './TreeNode'
import View from '../view'

interface State{}

interface Props{
  view: View
  recursive: boolean
  inspectedTree: fairygui.GObject[]
}

export default class Tree extends React.Component<Props, State> {
  render() {

    const elements = []
    let inspectedTree = []
    this.props.view._children && this.props.view._children.forEach((element, index) => {
      let highlight = false
        , folded = true
        , recursive = this.props.recursive

      if(element === this.props.inspectedTree[0]){
        inspectedTree = this.props.inspectedTree.slice(1)
        folded = false
        recursive = true
        if(!inspectedTree.length){
          highlight = true
          folded = true
        }
      }

      elements.push(
        <TreeNode 
          folded={folded} 
          node={element} 
          key={index} 
          recursive={recursive}
          inspectedTree={inspectedTree}
          highlight={highlight}
        />
      )
    })
    
    return <div className='tree'>{elements}</div>
  }
}