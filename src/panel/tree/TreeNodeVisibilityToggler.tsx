import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
library.add(faEye, faEyeSlash)
import View from '../view'

interface Props{
    node: View
}

interface State{
    visible: boolean
}

export default class TreeNodeVisibilityToggler extends React.Component<Props, State> {
    constructor(props){
        super(props)
        this.state = {
            visible: props.node.visible
        }
    }

    onClick = () => {
        this.props.node.visible = !this.props.node.visible
        this.setState({
            visible: this.props.node.visible
        })
    }

  render() {  
    const title = 'Toggle visibility ' + (this.state.visible ? 'OFF' : 'ON')
    return (
        <div className='tool visibility-toggler' title={title} onClick={this.onClick}>
            {this.state.visible ?
                <FontAwesomeIcon icon={faEye}/> :
                <FontAwesomeIcon icon={faEyeSlash}/>
            }
        </div>
    )
  }
}