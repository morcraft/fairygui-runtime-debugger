import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
library.add(faSearch)
import View from '../view'

interface Props{
  node: View
}

interface State{}

export default class TreeNodeLogger extends React.Component<Props, State> {
  onClick = () => {
    console.info(this.props.node)
  }

  render() {  
    return (
        <div className='logger tool' title='Show in console (Log)' onClick={this.onClick}>
            <FontAwesomeIcon icon={faSearch}/>
        </div>
    )
  }
}