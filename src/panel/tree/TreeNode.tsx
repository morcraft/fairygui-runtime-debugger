import * as React from 'react'
import DebugUtils from '../../utils'
import Tree from './Tree'
import TreeNodeChevron from './TreeNodeChevron'
import TreeNodeIcon from './TreeNodeIcon'
import TreeNodeLogger from './TreeNodeLogger'
import TreeNodeVisibilityToggler from './TreeNodeVisibilityToggler'
import TreeNodeChildren from './TreeNodeChildren'
import TreeNodeControllers from './TreeNodeControllers'
import TreeNodeLaya from './TreeNodeLaya'
import TreeNodeName from './TreeNodeName'
import TreeNodeTextToClipboard from './TreeNodeTextToClipboard'

interface State{
  node: fairygui.GComponent
  folded: boolean
  highlight: boolean
  recursive: boolean
  inspectedTree: fairygui.GComponent[]
}

interface Props{
  node: fairygui.GComponent
  folded: boolean
  highlight: boolean
  recursive: boolean
  inspectedTree: fairygui.GComponent[]
}

export default class TreeNode extends React.Component<Props, State> {
  constructor(props){
    super(props)
    this.state = {
      node: props.node,
      folded: props.folded,
      highlight: props.highlight,
      recursive: props.recursive,
      inspectedTree: props.inspectedTree
    }
  }

  componentWillReceiveProps (props){
    this.setState(props)
  }

  onClick = (event: React.MouseEvent<HTMLElement>) => {
    if(this.state.highlight){
      this.setState({
        highlight: false
      })
    }
  }

  onMouseEnter = (event: React.MouseEvent<HTMLElement>) => {
    DebugUtils.debugElement(this.state.node as fairygui.GComponent, {} as Laya.Event, false)
    if(event.currentTarget.classList.contains('highlight')){
      event.currentTarget.title = 'Click to remove highlight'
    }
  }

  onMouseLeave = (event) => {
    DebugUtils.cleanLines()
    DebugUtils.cleanPropertiesText()
  }

  onTreeNodeChevronClick = (event) => {
    this.setState({
      folded: !this.state.folded
    })
  }

  render() {
    const node = this.props.node
    const className = `tree-node ${this.state.highlight ? 'highlight' : ''}`

    return (
      <div className={className} onMouseEnter={this.onMouseEnter} onClick={this.onClick}>
        <TreeNodeChevron node={node} recursive={this.state.recursive} onClick={this.onTreeNodeChevronClick} folded={this.state.folded}/>
        <TreeNodeChildren node={node}/>
        <TreeNodeIcon node={node}/>
        <TreeNodeName node={node}/> 
        <TreeNodeControllers node={node}/>
        <TreeNodeLaya node={node}/>
        <TreeNodeLogger node={node}/>
        <TreeNodeVisibilityToggler node={node}/>
        <TreeNodeTextToClipboard node={node}/>

        {this.state.recursive && !this.state.folded ? 
          <Tree 
            view={node} 
            recursive={this.state.recursive} 
            inspectedTree={this.state.inspectedTree}/> : 
          null
        }
      </div>
    )
  }
}