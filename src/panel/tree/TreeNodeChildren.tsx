import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCodeBranch } from '@fortawesome/free-solid-svg-icons'
library.add(faCodeBranch)
import View from '../view'

interface Props{
  node: View
}

interface State{}

export default class TreeNodeChildrenNumber extends React.Component<Props, State> {
  onClick = (event) => {
    console.info(this.props.node._children)
  }

  render() {  
    if(!this.props.node || !Array.isArray(this.props.node._children) || !this.props.node._children.length)
      return null

    const number = this.props.node._children.length
    const title = `This node has ${number} ${(number > 1 ? 'children' : 'child')}. Click to view in console.`

    return(
        <div className='children' title={title} onClick={this.onClick}>
            <FontAwesomeIcon icon='code-branch'/>
            <span>{number}</span>
        </div>
    )
  }
}